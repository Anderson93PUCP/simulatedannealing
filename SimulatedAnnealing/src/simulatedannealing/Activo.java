/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulatedannealing;

/**
 *
 * @author a20140703
 */
public class Activo {
    private TipoVehiculo tipovehiculo;
    
    public Activo(){
        tipovehiculo = new TipoVehiculo(); 
    }

    /**
     * @return the tipovehiculo
     */
    public TipoVehiculo getTipovehiculo() {
        return tipovehiculo;
    }

    /**
     * @param tipovehiculo the tipovehiculo to set
     */
    public void setTipovehiculo(TipoVehiculo tipovehiculo) {
        this.tipovehiculo = tipovehiculo;
    }
}
