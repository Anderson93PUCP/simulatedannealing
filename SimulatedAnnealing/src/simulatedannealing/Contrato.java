/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulatedannealing;

import simulatedannealing.TipoMante;
import simulatedannealing.TipoVehiculo;

/**
 *
 * @author a20140703
 */
public class Contrato {
    private Proveedor proveedor;
    private TipoMante tipomantenimiento;
    private TipoVehiculo tipovehiculo;
    private int nivelComplejidad;
    private int capacidad;
    private int capacidadActual;
    private double tarifario;
    private double tiemporespuesta;
    private int estado;
    
    public Contrato(){
        proveedor=new Proveedor();
        tipomantenimiento= new TipoMante();
        tipovehiculo=new TipoVehiculo();
    }

    /**
     * @return the proveedor
     */
    public Proveedor getProveedor() {
        return proveedor;
    }

    /**
     * @param proveedor the proveedor to set
     */
    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    /**
     * @return the tipomantenimiento
     */
    public TipoMante getTipomantenimiento() {
        return tipomantenimiento;
    }

    /**
     * @param tipomantenimiento the tipomantenimiento to set
     */
    public void setTipomantenimiento(TipoMante tipomantenimiento) {
        this.tipomantenimiento = tipomantenimiento;
    }

    /**
     * @return the tipovehiculo
     */
    public TipoVehiculo getTipovehiculo() {
        return tipovehiculo;
    }

    /**
     * @param tipovehiculo the tipovehiculo to set
     */
    public void setTipovehiculo(TipoVehiculo tipovehiculo) {
        this.tipovehiculo = tipovehiculo;
    }

    /**
     * @return the nivelComplejidad
     */
    public int getNivelComplejidad() {
        return nivelComplejidad;
    }

    /**
     * @param nivelComplejidad the nivelComplejidad to set
     */
    public void setNivelComplejidad(int nivelComplejidad) {
        this.nivelComplejidad = nivelComplejidad;
    }

    /**
     * @return the capacidad
     */
    public int getCapacidad() {
        return capacidad;
    }

    /**
     * @param capacidad the capacidad to set
     */
    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    /**
     * @return the tarifario
     */
    public double getTarifario() {
        return tarifario;
    }

    /**
     * @param tarifario the tarifario to set
     */
    public void setTarifario(double tarifario) {
        this.tarifario = tarifario;
    }

    /**
     * @return the tiemporespuesta
     */
    public double getTiemporespuesta() {
        return tiemporespuesta;
    }

    /**
     * @param tiemporespuesta the tiemporespuesta to set
     */
    public void setTiemporespuesta(double tiemporespuesta) {
        this.tiemporespuesta = tiemporespuesta;
    }

    /**
     * @return the estado
     */
    public int getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(int estado) {
        this.estado = estado;
    }

    /**
     * @return the capacidadActual
     */
    public int getCapacidadActual() {
        return capacidadActual;
    }

    /**
     * @param capacidadActual the capacidadActual to set
     */
    public void setCapacidadActual(int capacidadActual) {
        this.capacidadActual = capacidadActual;
    }
    
    public void recuperarCapacidad(){
        this.capacidadActual += 1;
    }
    
    public void disminuirCapacidad(){
        this.capacidadActual -= 1;
    }

    
}
