/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulatedannealing;

import java.util.ArrayList;

/**
 *
 * @author andersoncastillo
 */
public class Contratos {
    private static ArrayList contratos = new ArrayList<Contrato>();

    // Adds a destination city
    public static void addContrato(Contrato sol) {
        contratos.add(sol);
    }
    
    // Get a city
    public static Contrato getContrato(int index){
        return (Contrato)contratos.get(index);
    }
    
    // Get the number of destination cities
    public static int numberOfContratos(){
        return contratos.size();
    }
}
