/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulatedannealing;

/**
 *
 * @author andersoncastillo
 */
public class ContratoxSolicitud {

    /**
     * @return the contrato
     */
    
    
    private Contrato contrato;
    private Solicitud solicitud;
    
    public ContratoxSolicitud(){
        contrato=new Contrato();
        solicitud=new Solicitud();
    }
    
    public Contrato getContrato() {
        return contrato;
    }

    /**
     * @param contrato the contrato to set
     */
    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    /**
     * @return the solicitud
     */
    public Solicitud getSolicitud() {
        return solicitud;
    }

    /**
     * @param solicitud the solicitud to set
     */
    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }
    
    @Override
    public String toString(){
        return getContrato().getProveedor().getNombrecontrato()+", "+getSolicitud().getCodigo();
    }
    
}
