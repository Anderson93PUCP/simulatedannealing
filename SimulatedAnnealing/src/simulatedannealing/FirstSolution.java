/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulatedannealing;

import java.util.ArrayList;

/**
 *
 * @author Anderson
 */
public class FirstSolution {
    private static ArrayList destinationCities = new ArrayList<ContratoxSolicitud>();

    // Adds a destination city
    public static void addSolucion(ContratoxSolicitud sol) {
        destinationCities.add(sol);
    }
    
    // Get a city
    public static ContratoxSolicitud getSolucion(int index){
        return (ContratoxSolicitud)destinationCities.get(index);
    }
    
    // Get the number of destination cities
    public static int numberOfSoluciones(){
        return destinationCities.size();
    }
}
