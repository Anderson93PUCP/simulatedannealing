/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulatedannealing;

/**
 *
 * @author Anderson
 */
import java.util.ArrayList;
import java.util.Collections;
import simulatedannealing.ContratoxSolicitud;

/**
 *
 * @author Anderson
 */
public class PossibleSolution {
    // Holds our tour of cities
    private ArrayList<ContratoxSolicitud> tour = new ArrayList<ContratoxSolicitud>();
    // Cache
    private int distance = 0;
    
    // Constructs a blank tour
    public PossibleSolution(){
        for (int i = 0; i < FirstSolution.numberOfSoluciones(); i++) {
            ContratoxSolicitud c=new ContratoxSolicitud();
            tour.add(c);
        }
    }
    
    // Constructs a tour from another tour
    public PossibleSolution(ArrayList tour){
        this.tour = (ArrayList) tour.clone();
    }
    
    // Returns tour information
    public ArrayList getSolution(){
        return tour;
    }
    
    public double FO(Contrato c, Solicitud s) {
        return (c.getTipovehiculo().getCriticidadOp() * s.getNivelcomplejidad() * s.getPrioridad() * c.getProveedor().getPromediocalificacion()) / c.getTiemporespuesta() * c.getTarifario();
    }

    // Creates a random individual
    //La solucion inicial debe ser por la FO
    public void generateIndividual() {
        
        // Loop through all our destination cities and add them to our tour
        for (int cityIndex = 0; cityIndex < FirstSolution.numberOfSoluciones(); cityIndex++) {
          setSolution(cityIndex, FirstSolution.getSolucion(cityIndex));
        }
        // Randomly reorder the tour
        Collections.shuffle(tour);
    }

    // Gets a city from the tour
    //Devuelve el grupo de Contrato x Solicitud asociados despues de la seleccion
    public ContratoxSolicitud getSolution(int tourPosition) {
        return (ContratoxSolicitud)tour.get(tourPosition);
    }

    // Sets a city in a certain position within a tour
    public void setSolution(int tourPosition, ContratoxSolicitud sol) {
        tour.set(tourPosition, sol);
        // If the tours been altered we need to reset the fitness and distance
        distance = 0;
    }
    
    //Imprime la solucion osea el Tour, al inicio y al final de elegir la mejor solucion
    public String firstSolution(){
        String solution=" ";
        for (int i = 0; i < FirstSolution.numberOfSoluciones(); i++) {
            solution += "Solicitud: ";
            String solicitud=FirstSolution.getSolucion(i).getSolicitud().getCodigo();
            String contrato=FirstSolution.getSolucion(i).getContrato().getProveedor().getNombrecontrato();
            solution+=solicitud;
            solution+="| Contrato: ";
            solution+=contrato;
            solution+=" \n";
        }
        return solution;
    }
    
    //Segun lo acordado, getDistance compara soluciones mediante un promedio
    //Este promedio es la multiplicacion de la prioridad de la solicitud  por el tiempo de respuesta
    //y se divide entre la cantidad de solcitudes, se debe mejorar esta funcion de calidad
    public double getCost(){
        double average=0;
        for (int i = 0; i < FirstSolution.numberOfSoluciones(); i++) {
            average += SimulatedAnnealing.FO(tour.get(i).getContrato(), tour.get(i).getSolicitud());
        }
        return average/FirstSolution.numberOfSoluciones();
    }
    
    
    // Get number of cities on our tour
    public int solutionSize() {
        return tour.size();
    }
    
    @Override
    public String toString() {
        String geneString = "|";
        for (int i = 0; i < solutionSize(); i++) {
            geneString += getSolution(i)+"|";
        }
        return geneString;
    }
}
