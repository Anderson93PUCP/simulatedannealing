/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulatedannealing;

import java.io.*;
import java.util.ArrayList;

/**
 *
 * @author andersoncastillo
 */
public class SimulatedAnnealing {

    public static double FO(Contrato c, Solicitud s) {
        return (c.getTipovehiculo().getCriticidadOp() * s.getNivelcomplejidad() * s.getPrioridad() * c.getProveedor().getPromediocalificacion()) 
                / c.getTiemporespuesta() * c.getTarifario();
    }
    

    public static double acceptanceProbability(double energy, double newEnergy, double temperature) {
        // If the new solution is better, accept it
        if (newEnergy < energy) {
            return 1.0;
        }
        // If the new solution is worse, calculate an acceptance probability
        return Math.exp((energy - newEnergy) / temperature);
    }
    
/*  Procedimiento que genera el PossibleSolution inicial, por llegada de solicitud ,
    asignando un Contrato segun su FO, una vez elegido un Contrato a una
    solicitud, este es sacado de la lista de Contratos */
    public static void generateFirstSolution(){
        double valueFO=0;
        double FOI=0;
        int index=0;
        int encontrado=0;

        for (int i = 0; i < Solicitudes.numberOfSolicitudes(); i++) {
            Solicitud s = Solicitudes.getSolicitud(i);
            valueFO=0;
            encontrado=0;
            for (int j = 0; j < Contratos.numberOfContratos(); j++) {
                Contrato c = Contratos.getContrato(j);
                if(c.getCapacidadActual() == 0)
                    continue;
                if(c.getNivelComplejidad() == s.getNivelcomplejidad()
                        && c.getTipovehiculo().getNombre().equals(s.getVehiculo().getTipovehiculo().getNombre()))
                {
                    encontrado=1;
                    FOI = FO(c, s);
                    if (FOI > valueFO) {
                        valueFO = FOI;
                        index = j;
                    }
                }
            }
            if(encontrado==1){
                int capacidadActual = Contratos.getContrato(index).getCapacidadActual() - 1;
                Contratos.getContrato(index).setCapacidadActual(capacidadActual);
                ContratoxSolicitud cxs=new ContratoxSolicitud();
                cxs.setContrato(Contratos.getContrato(index));
                cxs.setSolicitud(Solicitudes.getSolicitud(i));
                FirstSolution.addSolucion(cxs);
            }
        }
    }

    public static void main(String[] args) {
        
        /* Lee los archivos Contrato y Solicitud para agregarlos a listas de clase
            estaticas y utilizarlas en cada iteracion
        */
        String fileName = "../SimulatedAnnealing/Contratos.csv"; //d:/Users/andersoncastillo/Desktop/
        String fileName1 = "../SimulatedAnnealing/Solicitudes.csv";
        // This will reference one line at a time
        String line = null;

        for (int i = 0; i < 2; i++) {
            try {
                // FileReader reads text files in the default encoding.

                FileReader fileReader;
                if (i == 0) {
                    fileReader = new FileReader(fileName);
                } else {
                    fileReader = new FileReader(fileName1);
                }

                // Always wrap FileReader in BufferedReader.
                BufferedReader bufferedReader = new BufferedReader(fileReader);

                while ((line = bufferedReader.readLine()) != null) {
                    //System.out.println(line); 
                    String[] values = line.split(",");
                    //System.out.println(values[0]);
                    if (i == 0) {
                        Contrato contrato = new Contrato();
                        contrato.getProveedor().setNombrecontrato(values[0]);
                        contrato.getTipovehiculo().setCriticidadOp(Integer.parseInt(values[1]));
                        contrato.getProveedor().setPromediocalificacion(Integer.parseInt(values[2]));
                        contrato.setTiemporespuesta(Integer.parseInt(values[3]));
                        contrato.setTarifario(Integer.parseInt(values[4]));
                        contrato.setCapacidad(Integer.parseInt(values[5]));
                        contrato.setCapacidadActual(Integer.parseInt(values[5]));
                        contrato.getTipovehiculo().setNombre(values[6]);
                        contrato.setNivelComplejidad(Integer.parseInt(values[7]));
                        Contratos.addContrato(contrato);
                    } else {
                        Solicitud solicitud = new Solicitud();
                        solicitud.setCodigo(values[0]);
                        solicitud.setNivelcomplejidad(Integer.parseInt(values[1]));
                        solicitud.setPrioridad(Integer.parseInt(values[2]));
                        solicitud.getVehiculo().getTipovehiculo().setNombre(values[3]);      
                        Solicitudes.addSolicitud(solicitud);
                    }

                }

                // Always close files.
                bufferedReader.close();
            } catch (FileNotFoundException ex) {
                System.out.println("Unable to open file '" + fileName + "'");
            } catch (IOException ex) {
                System.out.println("Error reading file '" + fileName + "'");
                // Or we could just do this: 
                // ex.printStackTrace();
            }
        }

        // Set initial temp
        double temp = 10000;

        // Cooling rate
        double coolingRate = 0.003;

        // Initialize intial solution
        generateFirstSolution();
        PossibleSolution currentSolution = new PossibleSolution();
        currentSolution.generateIndividual(); //Genera la solucion inicial

        System.out.println("Initial solution distance: \n"+ currentSolution.firstSolution());
        // Set as current best
        PossibleSolution best = new PossibleSolution(currentSolution.getSolution());
        // Loop until system has cooled
         while (temp > 1) {
            // Create new neighbour tour
            PossibleSolution newSolution = new PossibleSolution(currentSolution.getSolution());
            int indexSolicitud = (int) (newSolution.solutionSize()* Math.random());
            int indexContrato;
            Solicitud s = newSolution.getSolution(indexSolicitud).getSolicitud();
            
            newSolution.getSolution(indexSolicitud).getContrato().recuperarCapacidad();
            Contrato c;
            int contador=0;
            ArrayList<Integer> listaIndex = new ArrayList<Integer>();
            int estado=0;
            while(true){
                indexContrato = (int) (Contratos.numberOfContratos()* Math.random());//newSolution.tourSize()
                if(!listaIndex.contains(indexContrato)){
                    listaIndex.add(indexContrato);
                    contador++;
                }
                c = Contratos.getContrato(indexContrato);
                if (c.getCapacidadActual() == 0 ) {
                    continue;
                }
                if (c.getNivelComplejidad() == s.getNivelcomplejidad()
                        && c.getTipovehiculo().getNombre().equals(s.getVehiculo().getTipovehiculo().getNombre())) {
                    estado=1;
                    break;
                }
                if(contador==Contratos.numberOfContratos())
                    break;
            }
            if(estado==1){
                c.disminuirCapacidad();
                ContratoxSolicitud cs = newSolution.getSolution(indexSolicitud);
                cs.setContrato(c);

                //newSolution.getCity(indexSolicitud).setContrato(c);


                // Get energy of solutions
                double currentEnergy = currentSolution.getCost();
                double neighbourEnergy = newSolution.getCost();

                // Decide if we should accept the neighbour
                if (acceptanceProbability(currentEnergy, neighbourEnergy, temp) > Math.random()) {
                    currentSolution = new PossibleSolution(newSolution.getSolution());
                }

                // Keep track of the best solution found
                if (currentSolution.getCost() < best.getCost()) {
                    best = new PossibleSolution(currentSolution.getSolution());
                }
            }
            // Cool system
            temp *= 1 - coolingRate;
        }

        System.out.println("Final solution distance: \n" + best.firstSolution());
        //System.out.println("PossibleSolution: " + best);
    }

}