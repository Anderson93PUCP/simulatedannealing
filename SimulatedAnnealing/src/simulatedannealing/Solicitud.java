/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulatedannealing;

import simulatedannealing.Activo;
import simulatedannealing.Contrato;

/**
 *
 * @author a20140703
 */
public class Solicitud  implements Comparable<Solicitud>{
    private String codigo;
    private TipoMante tipoM;
    private Activo vehiculo;
    private Contrato contrato;
    private int nivelcomplejidad;
    private int prioridad;
    private Usuario usuario;
    private String fechaEmision;
    private String fechaAtencion;
    private String fechaFinal;
    private int calificacion;
    private int estadosolicitud;
    private int estado;
    
    public Solicitud(){
        tipoM=new TipoMante();
        vehiculo=new Activo();
        contrato=new Contrato();
        usuario=new Usuario();
        
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the tipoM
     */
    public TipoMante getTipoM() {
        return tipoM;
    }

    /**
     * @param tipoM the tipoM to set
     */
    public void setTipoM(TipoMante tipoM) {
        this.tipoM = tipoM;
    }

    /**
     * @return the vehiculo
     */
    public Activo getVehiculo() {
        return vehiculo;
    }

    /**
     * @param vehiculo the vehiculo to set
     */
    public void setVehiculo(Activo vehiculo) {
        this.vehiculo = vehiculo;
    }

    /**
     * @return the proveedor
     */
    public Contrato getContrato() {
        return contrato;
    }

    /**
     * @param proveedor the proveedor to set
     */
    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    /**
     * @return the nivelcomplejidad
     */
    public int getNivelcomplejidad() {
        return nivelcomplejidad;
    }

    /**
     * @param nivelcomplejidad the nivelcomplejidad to set
     */
    public void setNivelcomplejidad(int nivelcomplejidad) {
        this.nivelcomplejidad = nivelcomplejidad;
    }

    /**
     * @return the prioridad
     */
    public int getPrioridad() {
        return prioridad;
    }

    /**
     * @param prioridad the prioridad to set
     */
    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }

    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the fechaEmision
     */
    public String getFechaEmision() {
        return fechaEmision;
    }

    /**
     * @param fechaEmision the fechaEmision to set
     */
    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    /**
     * @return the fechaAtencion
     */
    public String getFechaAtencion() {
        return fechaAtencion;
    }

    /**
     * @param fechaAtencion the fechaAtencion to set
     */
    public void setFechaAtencion(String fechaAtencion) {
        this.fechaAtencion = fechaAtencion;
    }

    /**
     * @return the fechaFinal
     */
    public String getFechaFinal() {
        return fechaFinal;
    }

    /**
     * @param fechaFinal the fechaFinal to set
     */
    public void setFechaFinal(String fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    /**
     * @return the calificacion
     */
    public int getCalificacion() {
        return calificacion;
    }

    /**
     * @param calificacion the calificacion to set
     */
    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }

    /**
     * @return the estadosolicitud
     */
    public int getEstadosolicitud() {
        return estadosolicitud;
    }

    /**
     * @param estadosolicitud the estadosolicitud to set
     */
    public void setEstadosolicitud(int estadosolicitud) {
        this.estadosolicitud = estadosolicitud;
    }

    /**
     * @return the estado
     */
    public int getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(int estado) {
        this.estado = estado;
    }

    @Override
    public int compareTo(Solicitud o) {
        return prioridad-o.getPrioridad();
    }
}
