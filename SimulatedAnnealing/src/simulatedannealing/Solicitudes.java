/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulatedannealing;

import java.util.ArrayList;

/**
 *
 * @author andersoncastillo
 */
public class Solicitudes {
    private static ArrayList solicitudes = new ArrayList<Solicitud>();

    // Adds a destination city
    public static void addSolicitud(Solicitud sol) {
        solicitudes.add(sol);
    }
    
    // Get a city
    public static Solicitud getSolicitud(int index){
        return (Solicitud)solicitudes.get(index);
    }
    
    // Get the number of destination cities
    public static int numberOfSolicitudes(){
        return solicitudes.size();
    }
}
