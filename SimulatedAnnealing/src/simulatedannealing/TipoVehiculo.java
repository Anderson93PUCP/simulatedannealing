/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulatedannealing;

/**
 *
 * @author a20140703
 */
public class TipoVehiculo {
    private String nombre;
    private int criticidadOp;
    
    public TipoVehiculo(){
    
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the criticidadOp
     */
    public int getCriticidadOp() {
        return criticidadOp;
    }

    /**
     * @param criticidadOp the criticidadOp to set
     */
    public void setCriticidadOp(int criticidadOp) {
        this.criticidadOp = criticidadOp;
    }
}
